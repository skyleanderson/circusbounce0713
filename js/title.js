game.TitleScreen = me.ScreenObject.extend({
	/**	
	 *  action to perform on state change
	 */
	onResetEvent: function() {	
	    this.title = new me.Font("Lucida Console, monospace", 36, "#fff", "left");
	    this.prompt = new me.Font("Lucida Console, monospace", 16, "#bbb", "left");
	    me.game.add(new me.ColorLayer("bg", "#000000", 1));
	    this.cooldown = 30;
	    this.showPrompt = true;
	},

	init: function()
	{
	    this.parent(true);
	},
	
	draw: function(context)
	{
	    var size = this.title.measureText(context, "CIRCUS BOUNCE");
	    this.title.draw(context, "CIRCUS BOUNCE", (me.video.getWidth() * .5) - (size.width * .5), 100);

	    if (me.gamestat.getItemValue("lastScore") > 0)
	    {
	        size = this.prompt.measureText(context, "LAST: " + me.gamestat.getItemValue("lastScore") + " HIGH: " + me.gamestat.getItemValue("highScore"));
	        this.prompt.draw(context, "LAST: " + me.gamestat.getItemValue("lastScore") + " HIGH: " + me.gamestat.getItemValue("highScore"), (me.video.getWidth() * .5) - (size.width * .5), 150);
	    }

	    if (this.showPrompt)
	    {
	        size = this.prompt.measureText(context, "CLICK TO BEGIN");
	        this.prompt.draw(context, "CLICK TO BEGIN", (me.video.getWidth() * .5) - (size.width * .5), 260);
	    }
	},

	update: function()
	{
	    this.cooldown--;
	    if (this.cooldown <= 0)
	    {
	        this.cooldown = 30;
	        this.showPrompt = !this.showPrompt;
        }
	    if (me.input.isKeyPressed("enter") || me.input.isKeyPressed("drag"))
	    {
	        me.audio.play("start");
	        me.state.change(me.state.PLAY);
	    }
	},

	/**	
	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent: function() {
	  ; // TODO
	}
});
