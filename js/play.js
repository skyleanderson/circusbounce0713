game.PlayScreen = me.ScreenObject.extend({
	/**	
	 *  action to perform on state change
	 */
    onResetEvent: function ()
    {
	    me.levelDirector.loadLevel("level");
	    me.game.add(new me.ColorLayer("bg", "#000000", 1));

	    me.gamestat.add("state", 0);
	    me.gamestat.add("score", 0);
	    me.gamestat.add("lives", 3);

	    me.gamestat.setValue("state", 0);
	    me.gamestat.setValue("score", 0);
	    me.gamestat.setValue("lives", 3);
    },
	
	
	/**	
	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent: function() {
	  ; // TODO
	}
});
