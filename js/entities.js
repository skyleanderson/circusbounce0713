/*********************************************************
CONSTANTS
*********************************************************/
var CONSTANTS = {
    "BOUNCE_SPEED_BASE"  : 10,
    "BOUNCE_SPEED_BONUS" : 6,
    "BOUNCE_X_SPREAD"    : 8,
    "GRAVITY"            : .66,
    "BALLOON_SPEED"      : 1.4,
    "BALLOONS_PER_ROW"   : 20,
    "BALLOON_PUSH_SPEED" : 4,
    "BOUNCE_INIT_X"      : 4,
    "BOUNCE_INIT_Y"      : 10,

    "SPAWN_STATE"        : 0,
    "PLAY_STATE"         : 1,
    "DEAD_STATE"         : 2,
    "DEATH_COOLDOWN": 60,
    "MESSAGE_COOLDOWN": 30
}

/*********************************************************
PADDLE ENTITY
*********************************************************/
var PaddleEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        // set the walking & jumping speed

        //set animations from the tilemap
        settings.image = "paddle"
        settings.spritewidth  = 128;
        settings.spriteheight = 32;
        this.parent(x, y, settings);
        this.type = me.game.PLAYER_OBJECT;

        this.updateColRect(0, 128, 0, 32);
        this.setVelocity(31, 0);
        this.jumping = false;
        this.falling = false;

        this.leftdown = false;
        this.renderable.addAnimation("rightDown", [0,0,1,1]);
        this.renderable.addAnimation("leftDown", [2,2,3,3]);
        this.renderable.setCurrentAnimation("rightDown");

        this.messageFont = new me.Font("Lucida Console, monospace", 12, "#bbff00", "left");
        this.messagePos = new me.Vector2d(0, 0);
        this.messageCooldown = 0;
        this.ui = new UIEntity(new me.Vector2d(0, 0), 100, 32);
        this.ui.init();
    },

    update: function ()
    {
        this.ui.update();

        if (this.messageCooldown > 0)
        {
            this.messageCooldown--;
            this.messagePos.y--;
        }

        //KEYBOARD
        if (me.input.isKeyPressed("left"))
        {
            this.vel.x = -8 * me.timer.tick;
        } else if (me.input.isKeyPressed("right"))
        {
            this.vel.x =8 * me.timer.tick;
        } else
        {
            this.vel.x = 0;
        }


        if (me.gamestat.getItemValue("state") != CONSTANTS.SPAWN_STATE &&
            me.input.isKeyPressed("enter"))
        {
            if (this.leftdown)
            {
                this.leftdown = false;
                this.renderable.setCurrentAnimation("rightDown");
            } else
            {
                this.leftdown = true;
                this.renderable.setCurrentAnimation("leftDown");
            }
        } else if (me.gamestat.getItemValue("state") == CONSTANTS.SPAWN_STATE &&
            (me.input.isKeyPressed("enter") || me.input.isKeyPressed("drag")))
        {
            //start the game
            me.gamestat.setValue("state", CONSTANTS.PLAY_STATE);
        }

        //MOUSE
        //if (
        //    me.input.mouse.pos.y >= this.pos.y - 32)
        {
            this.vel.x = me.input.mouse.pos.x - (this.pos.x + this.collisionBox.width * .5);
        }


        this.updateMovement();


        if (me.gamestat.getItemValue("state") == CONSTANTS.PLAY_STATE)
        {
             //check for collisions with the performer
            var res = me.game.collideType(this, me.game.ACTION_OBJECT);
            if (res) // collision
            {
                if (this.leftdown)
                {
                    // see if the collision was on the right half of the paddle
                    if (res.obj.pos.x >= this.pos.x + (.25 * this.collisionBox.width))
                    {
                        //good collision
                        this.leftdown = false;
                        this.renderable.setCurrentAnimation("rightDown");
                        me.gamestat.updateValue("score", 7);
                        me.audio.play("jump");

                        //accuracy is in [-4, 1]. [0-1] is perfect, -[4-3) bad -[3-2) ok -[2,1) good -[1,0) great
                        var accuracy = ((this.pos.x + this.collisionBox.width) - (res.obj.collisionBox.pos.x + res.obj.collisionBox.width)) / res.obj.collisionBox.width;
                        this.updateVelByAccuracy(accuracy, res.obj);
                    
                        //update the position to the other side (switching performers ;))
                        res.obj.pos.x = this.pos.x;
                        res.obj.pos.y = this.pos.y - res.obj.collisionBox.height - 1;
                    }
                } else
                {
                    if (res.obj.pos.x <= this.pos.x + (.75 * this.collisionBox.width))
                    {
                        this.leftdown = true;
                        this.renderable.setCurrentAnimation("leftDown");
                        me.gamestat.updateValue("score", 7);
                        me.audio.play("jump");

                        //accuracy is in [-6, 1]. [0-1] is perfect, -[6-3) bad -[3-2) ok -[2,1) good -[1,0) great
                        var accuracy = (this.pos.x - res.obj.collisionBox.pos.x) / res.obj.collisionBox.width;
                        this.updateVelByAccuracy(accuracy, res.obj);
                        this.vel.x *= -1;

                        //hit the paddle
                        res.obj.pos.x = this.pos.x + (this.collisionBox.width) - res.obj.collisionBox.width;
                        res.obj.pos.y = this.pos.y - res.obj.collisionBox.height - 1;
                    }
                }
            }
        }

        this.parent(this);
        return true;
    },

    // TODO NEEDS MORE TESTING!!!
    updateVelByAccuracy: function (accuracy, target)
    {
        //console.log(accuracy);
        //hit the paddle
        if (accuracy >= 0)
        {
            // perfect range
            target.vel.x = .5 * CONSTANTS.BOUNCE_X_SPREAD * (.5*accuracy);
            target.vel.y = -CONSTANTS.BOUNCE_SPEED_BASE;
            target.vel.y -= (CONSTANTS.BOUNCE_SPEED_BONUS);
            target.vel.y -= (2 * (1 - accuracy)); //full height + bonus
            this.message = "PERFECT!";
            me.gamestat.updateValue("score", 7);
        } else if (accuracy < 0 && accuracy >= -1)
        {
            //great
            accuracy *= -1;
            target.vel.x = CONSTANTS.BOUNCE_X_SPREAD * (.5*accuracy);
            target.vel.y = -CONSTANTS.BOUNCE_SPEED_BASE - (.75 * CONSTANTS.BOUNCE_SPEED_BONUS); //3/4 height
            this.message = " GREAT! ";
            me.gamestat.updateValue("score", 5);
        } else if (accuracy < -1 && accuracy >= -2)
        {
            //good
            accuracy = (-1 * accuracy) - 1;
            target.vel.x = CONSTANTS.BOUNCE_X_SPREAD * (accuracy);
            target.vel.y = -CONSTANTS.BOUNCE_SPEED_BASE - (.5 * CONSTANTS.BOUNCE_SPEED_BONUS); //half height
            this.message = "  GOOD  ";
            me.gamestat.updateValue("score", 3);
        } else if (accuracy < -2 && accuracy >= -3)
        {
            //ok
            accuracy = (-1 * accuracy) - 2;
            target.vel.x = CONSTANTS.BOUNCE_X_SPREAD * (accuracy);
            target.vel.y = -CONSTANTS.BOUNCE_SPEED_BASE - (.25 * CONSTANTS.BOUNCE_SPEED_BONUS); //quarter height
            this.message = "   OK   ";
            me.gamestat.updateValue("score", 1);
        } else // [-6,-3)
        {
            //bad
            target.vel.x = CONSTANTS.BOUNCE_X_SPREAD;
            target.vel.y = -CONSTANTS.BOUNCE_SPEED_BASE; //low height
            this.message = "  ....  ";
        }
        this.messageCooldown = CONSTANTS.MESSAGE_COOLDOWN;
        this.messagePos.x = this.pos.x - 32;
        this.messagePos.y = this.pos.y;

    },

    draw: function (context)
    {
        if (this.messageCooldown > 0)
        {
            this.messageFont.draw(context, this.message, this.messagePos.x, this.messagePos.y);
        }
        this.parent(context);
        this.ui.draw(context);
    }
}
);

/*********************************************************
BALLOONS
*********************************************************/
var BalloonAreaEntity = me.ObjectEntity.extend(
{


    init: function (x, y, settings)
    {
        this.parent(x, y, settings);

        this.hasActiveBalloons = true;
        this.balloonCount =  0;
        this.balloonList = [];
        //make 20 balloons

        for (var i = 0; i < 20; i++)
        {
            var balloon = me.entityPool.newInstanceOf("balloon", (32 * i), y, settings);
            this.balloonList.push(balloon);
        }
        this.balloonCount = this.balloonList.length;
        this.id = settings.id;
        me.game.sort();
    },

    update: function ()
    {
        this.hasActiveBalloons = false;
        for (var i = 0; i < this.balloonCount; i++)
        {
            this.balloonList[i].update();
            if (this.balloonList[i].alive)
            {
                this.hasActiveBalloons = true;
            }
        }

        //if all the balloons are dead, respawn a new row
        if (!this.hasActiveBalloons)
        {
            me.gamestat.updateValue("score", 500 * this.id);
            for (var i = 0; i < this.balloonCount; i++)
            {
                this.balloonList[i].alive      = true;
            }
        }
    },

    draw: function (context, x, y)
    {
        for (var i = 0; i < this.balloonCount; i++)
        {
            this.balloonList[i].draw(context);
        }
    }
});

var BalloonEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        var imageString = "balloon" + settings.id;
        settings.image = imageString;
        //settings.image = "balloon";
        settings.spriteheight = 16
        settings.spritewidth  = 16;
        this.parent(x, y, settings);
        me.game.add(this, 3);

        this.setVelocity(CONSTANTS.BALLOON_SPEED, 0);
        this.jumping = false;
        this.falling = false;
        this.alive = true;

        if (settings.id % 2 == 0)
        {
            this.vel.x = CONSTANTS.BALLOON_SPEED;
            this.flipX(true);
        } else
        {
            this.vel.x = -CONSTANTS.BALLOON_SPEED;
        }
        this.direction = new me.Vector2d(0, 0);
    },

    update: function ()
    {
        this.parent(this);
        this.pos.x += this.vel.x * me.timer.tick;
        if (this.pos.x >= me.video.getWidth())
        {
            this.pos.x = 0;
        } else if (this.pos.x < 0)
        {
            this.pos.x = me.video.getWidth();
        }

        if (this.alive == false)
            return true;

        var res = me.game.collideType(this, me.game.ACTION_OBJECT);
        if (res)
        {
            //push the performer in the direction he came from..
            this.direction.x = res.obj.collisionBox.pos.x - this.pos.x;
            this.direction.y = res.obj.collisionBox.pos.y - this.pos.y;
            this.direction.normalize();
            this.direction.x *= CONSTANTS.BALLOON_PUSH_SPEED;
            this.direction.y *= CONSTANTS.BALLOON_PUSH_SPEED;
            if (this.direction.y > 0) this.direction.y *= -1;
            res.obj.vel.add(this.direction);
            
            me.gamestat.updateValue("score", 50);
            me.audio.play("hit");
            this.alive = false;
        }
        return true;
    },

    draw: function (context, x, y)
    {
        if (this.alive)
            this.parent(context, x, y);
    }
});

/*********************************************************
PERFORMER ENTITY - this is the 'ball' that gets bounced around
*********************************************************/
var PerformerEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        settings.image = "performer"
        settings.spritewidth = 24;
        settings.spriteheight = 24;
        this.parent(x,y,settings);
        this.type = me.game.ACTION_OBJECT;

        this.startPosition = new me.Vector2d(x, y);
        // adjust the bounding box
        this.updateColRect(4, 16, 4, 16);
        this.setVelocity(6, CONSTANTS.BOUNCE_SPEED_BASE + (2*CONSTANTS.BOUNCE_SPEED_BONUS));
        this.gravity = 0;

        this.prevVelX = 0;

        this.deathCooldown = 0;


        this.renderable.addAnimation("stand", [8, 9, 10, 11, 10, 9]);
        this.renderable.addAnimation("jumpRight", [0, 1, 2, 3, 4, 5, 6, 7]);
        this.renderable.addAnimation("jumpLeft", [7, 6, 5, 4, 3, 2, 1, 0]);
        this.renderable.addAnimation("death", [12, 13, 14, 15]);
        this.renderable.setCurrentAnimation("stand");
    },

    update: function (x, y, settings)
    {


        //set animations
        //if (this.vel.x != this.prevVelX)
        //{
        switch (me.gamestat.getItemValue("state"))
        {
            case CONSTANTS.PLAY_STATE:
                this.gravity = CONSTANTS.GRAVITY;
                this.prevVelX = this.vel.x;

                this.updateMovement();

                if (this.vel.y != 0)
                {
                    if (this.pos.x <= 0 || this.pos.x >= (me.video.getWidth() - this.collisionBox.width))
                    {
                        this.vel.x = -this.prevVelX;
                    }
                }

                if (this.vel.x > .8)
                {
                    this.renderable.setCurrentAnimation("jumpRight");
                } else if (this.vel.x < -.8)
                {
                    this.renderable.setCurrentAnimation("jumpLeft");
                } else
                {
                    this.renderable.setCurrentAnimation("stand");
                }

                if (this.vel.y == 0 && this.pos.y > (me.video.getHeight() * .85) ||
                    this.pos.y > me.video.getHeight() - 16)
                {
                    //die
                    this.vel.y = 0;
                    this.vel.x = 0;
                    this.pos.y = me.video.getHeight() - 36;
                    this.deathCooldown = CONSTANTS.DEATH_COOLDOWN;
                    me.gamestat.setValue("state", CONSTANTS.DEAD_STATE);
                    me.gamestat.updateValue("lives", -1);
                    me.audio.play("death");
                }

                break;
            case CONSTANTS.SPAWN_STATE:
                this.pos.set(this.startPosition.x, this.startPosition.y);
                this.renderable.setCurrentAnimation("stand");
                this.vel.set(0, 0);
                break;
            case CONSTANTS.DEAD_STATE:
                this.deathCooldown -= me.timer.tick;
                this.renderable.setCurrentAnimation("death");

                if (this.deathCooldown <= 0)
                {
                    this.deathCooldown = 0;
                    if (me.gamestat.getItemValue("lives") > 0)
                    {
                        me.gamestat.setValue("state", CONSTANTS.SPAWN_STATE);
                    } else
                    {
                        me.gamestat.setValue("lastScore", me.gamestat.getItemValue("score"));
                        me.state.change(me.state.MENU);
                    }
                }
                break;
        }

        //}


        this.parent(true);
        return true;
    }
});


/*********************************************************
TRAMPOLINE ENTITY - this is used to keep the performer in play
*********************************************************/
var TrampolineEntity = me.ObjectEntity.extend(
{
    init: function (x,y,settings)
    {
        settings.image = "map"
        settings.spriteheight = settings.height;
        settings.spritewidth = settings.width;
        this.parent(x, y, settings);
        this.type = me.game.ENEMY_OBJECT;
        this.visible = true;
        this.collidable = true;
    },

    update: function ()
    {
        var res = me.game.collide(this);
        if (res)
        {
            if (res.type == me.game.ACTION_OBJECT && res.obj.vel.y > 0)
            {
                if (this.pos.x < 100)
                {
                    res.obj.vel.x = CONSTANTS.BOUNCE_INIT_X;
                } else
                {
                    res.obj.vel.x = -CONSTANTS.BOUNCE_INIT_X;
                }
                res.obj.vel.y = -CONSTANTS.BOUNCE_INIT_Y;
                me.audio.play("jump");
            }
        }
    },

    draw: function(context, x, y)
    {
        return;
    }
});

var UIEntity = me.ObjectEntity.extend(
{
    init: function()
    {
        this.score = new me.Font("Lucida Console, monospace", 22, "#fff", "left");
        this.high = new me.Font("Lucida Console, monospace", 22, "#bbb", "right");
        var settings = {};
        settings.image = "performer";
        settings.spritewidth = 24;
        settings.spriteheight = 24;
        this.parent(0, 0, settings);
        this.renderable.addAnimation("stand", [8, 9, 10, 11, 10, 9]);
        this.renderable.setCurrentAnimation("stand");
        this.pos.x = (me.video.getWidth() * .5) - 24;
        this.pos.y = 0;
    },

    update: function()
    {
        if (me.gamestat.getItemValue("score") > me.gamestat.getItemValue("highScore"))
        {
            me.gamestat.setValue("highScore", me.gamestat.getItemValue("score"));
        }
        this.parent();
        return true;
    },

    draw: function (context)
    {
        this.parent(context);
        this.score.draw(context, " x " + me.gamestat.getItemValue("lives"), this.pos.x + 24, 4);
        this.score.draw(context, me.gamestat.getItemValue("score"), 4, 4);
        var highWidth = this.high.measureText(context, me.gamestat.getItemValue("highScore"));
        this.high.draw(context, me.gamestat.getItemValue("highScore"), me.video.getWidth() - 4, 4);
    }
}
);

