
/* Game namespace */
var game = {
    // Run on page load.
    "onload" : function () {
        // Initialize the video.
        if (!me.video.init("screen", 640, 320, true, 'auto', true))
        {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }
		
		// add "#debug" to the URL to enable the debug Panel
		if (document.location.hash === "#debug") {
			window.onReady(function () {
				me.plugin.register.defer(debugPanel, "debug");
			});
		}

        // Initialize the audio.
        me.audio.init("mp3,ogg,wav");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);
     
        // Load the resources.
        me.loader.preload(game.resources);

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
    },



    // Run on game resources loaded.
    "loaded": function ()
    {
        //TODO: REMOVE DEBUG
        //me.debug.renderHitBox = true;

        me.sys.fps = 30;

        me.entityPool.add("paddle", PaddleEntity);
        me.entityPool.add("performer", PerformerEntity);
        me.entityPool.add("balloonArea", BalloonAreaEntity);
        me.entityPool.add("balloon", BalloonEntity);
        me.entityPool.add("trampoline", TrampolineEntity);

        me.state.set(me.state.MENU, new game.TitleScreen());
        me.state.set(me.state.PLAY, new game.PlayScreen());

        // enable the keyboard
        me.input.bindKey(me.input.KEY.LEFT, "left");
        me.input.bindKey(me.input.KEY.RIGHT, "right");
        me.input.bindKey(me.input.KEY.UP,  "drag", true);
        me.input.bindKey(me.input.KEY.ENTER, "enter", true);
        me.input.bindMouse(me.input.mouse.LEFT, me.input.KEY.UP);
        
        me.gamestat.add("highScore", 10000);
        me.gamestat.add("lastScore", 0);

        // Start the game.
        me.state.change(me.state.MENU);
    }
};
