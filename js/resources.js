game.resources = [

	/* Graphics. 
	 * @example
	 * {name: "example", type:"image", src: "data/img/example.png"},
	 */
	{ name: "paddle",         type: "image", src: "res/img/paddle.png" },
	{ name: "performer", type: "image", src: "res/img/performer.png" },
	{ name: "balloon1", type: "image", src: "res/img/balloon1.png" },
	{ name: "balloon2", type: "image", src: "res/img/balloon2.png" },
	{ name: "balloon3", type: "image", src: "res/img/balloon3.png" },
	{ name: "map", type: "image", src: "res/img/map.png" },
	{ name: "metatiles16x16", type: "image", src: "res/img/metatiles16x16.png" },

    	
	/* Maps. 
	 * @example
	 * {name: "example01", type: "tmx", src: "data/map/example01.tmx"},
	 * {name: "example01", type: "tmx", src: "data/map/example01.json"},
 	 */
	{ name: "level", type: "tmx", src: "res/map.tmx" },
	
	/* Sound effects. 
	 * @example
	 * {name: "example_sfx", type: "audio", src: "data/sfx/", channel : 2}
	 */
    {name: "hit",   type: "audio", src: "res/sfx/", channel : 1},
    {name: "death", type: "audio", src: "res/sfx/", channel : 1},
    {name: "jump", type: "audio", src: "res/sfx/", channel : 1},
    {name: "start", type: "audio", src: "res/sfx/", channel : 1}
];
