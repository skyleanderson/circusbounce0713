Circus Bounce 0713
-------------------------------------------------------------------------------

Remake of Circus Atari.
Made for OneGameAMonth, July 2013, using melonJS.

-------------------------------------------------------------------------------
Copyright (C) 2013 S Kyle Anderson
melonJS is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php)